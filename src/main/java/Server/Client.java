package Server;

import com.github.javafaker.Faker;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.*;
import java.io.*;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.Objects;
import java.util.Scanner;
import java.util.stream.Stream;

import static java.nio.file.StandardWatchEventKinds.*;

public class Client {

  private String lastSelectedNode;
  private String lastSelectedUser;

  private DefaultMutableTreeNode root;

  private static DefaultTreeModel model;

  private static JTree tree;
  private JLabel statusLabel;

  private JComboBox userList;

  private ArrayList<String> listOfUsers;

  JFrame frame = new JFrame("File Browser");

  static String username;
  static String userPath;
  private static Socket clientSocket;
  private PrintWriter out;
  private static BufferedReader in;

  /**
   * Checks if given folder exists
   * @param folderName folder to be checked
   * @return false if folder doesn't exist, true if exists
   */
  public static boolean checkIfFolderExists(String folderName) {

    boolean found = false;

    try {
      File file = new File(folderName);
      if (file.exists() && file.isDirectory()) {
        found = true;
      }
    } catch (Exception e) {
      e.printStackTrace();
    }

    return found;
  }

  /**
   * Establishes connection with server. Function called at the beginning.
   * @param ip IP address that Client connects to
   * @param port Port number that Client connects to
   * @param name Client's name
   * @throws IOException In case of Writer/Reader problems
   */

  public void startConnection(String ip, int port, String name) throws IOException {
    try {
      while (true) {
        clientSocket = new Socket(ip, port);
        out = new PrintWriter(clientSocket.getOutputStream(), true);
        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        sendMessage(username);
        Path rootDirectory = FileSystems.getDefault().getPath("src/ClientFiles");
        String expectedDir = rootDirectory + "/" + username;

        if (checkIfFolderExists(expectedDir)) {
          System.out.println("Folder present: " + checkIfFolderExists(expectedDir));
          System.out.println(expectedDir);
        } else {
          System.out.println("Folder present: " + checkIfFolderExists(expectedDir));
          Path userDirectory = Files.createDirectory(Paths.get(rootDirectory + "/" + username));
          System.out.println(userDirectory);
        }
        break;
      }
      startGUI();
    } catch (Exception exc) {
      exc.printStackTrace();
    }
  }

  /**
   * Ends connection with the server
   */
  public void stopConnection() {
    try {
      sendMessage("Disconnected");
      in.close();
      out.close();
      clientSocket.close();
    } catch (Exception e) {
      System.out.println(e.getMessage());
    }
  }

  /**
   * Sends message to the server using PrintWriter
   * @param msg message to be send to Server
   */

  public void sendMessage(String msg) {
    try {
      out.println(msg);
    } catch (Exception exc) {
      System.out.println("ERROR - " + exc);
    }
  }

  /**
   * Function monitors for any changes in given directory
   * @param path path to directory that needs to be monitored
   * @throws IOException in case of problems with sending message or running watcher
   */
  public void watchService(String path) throws IOException {
    WatchService watcher = FileSystems.getDefault().newWatchService();
    Path dir = Paths.get(path);
    dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

    while (true) {
      WatchKey key;
      try {
        // wait for a key to be available
        key = watcher.take();
      } catch (InterruptedException ex) {
        return;
      }

      for (WatchEvent<?> event : key.pollEvents()) {
        // get event type
        WatchEvent.Kind<?> kind = event.kind();

        // get file name
        @SuppressWarnings("unchecked")
        WatchEvent<Path> ev = (WatchEvent<Path>) event;
        Path fileName = ev.context();

        if (kind == OVERFLOW) {
          continue;
        } else if (kind == ENTRY_CREATE) {
          update();
          sendMessage("New File");
          statusLabel.setText("Sending...");
          sendMessage(fileName.toString());
          sendFile(path, fileName);

        } else if (kind == ENTRY_DELETE) {
          update();
          sendMessage("Deleted File");
          sendMessage(fileName.toString());

        } else if (kind == ENTRY_MODIFY) {
        }
        statusLabel.setText("Checking...");
      }
      // IMPORTANT: The key must be reset after processed
      boolean valid = key.reset();
      if (!valid) {
        break;
      }
    }
  }

  /**
   * Sends given file to given directory using Input/Output stream
   * @param path path to the file that need to be sent
   * @param filename name of file that need to be send
   * @throws IOException in case of problems with  Input/Output Stream
   */
  public void sendFile(String path, Path filename) throws IOException {
    FileInputStream fileInput = null;
    BufferedInputStream bufferInput = null;
    OutputStream dataOut = null;
    File myFile = new File(path + File.separator + filename);
    byte[] byteArray = new byte[(int) myFile.length()];

    fileInput = new FileInputStream(myFile);
    bufferInput = new BufferedInputStream(fileInput);
    bufferInput.read(byteArray, 0, byteArray.length);
    dataOut = clientSocket.getOutputStream();
    System.out.println("Sending " + byteArray.length + " bytes");

    dataOut.write(byteArray, 0, byteArray.length);
    System.out.println("Done");
    bufferInput.close();
  }

  /**
   * Saves given file using Input/Output Stream
   * @param filepath full path to the directory where file need to be saved
   * @throws IOException in case of problems with  Input/Output Stream
   */
  public static void saveFile(String filepath) throws IOException {
    FileOutputStream fileOutput = new FileOutputStream(filepath);
    BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
    int bytesRead;
    int current = 0;
    byte[] byteArray = new byte[8192];
    InputStream dataIn = clientSocket.getInputStream();
    bytesRead = dataIn.read(byteArray, 0, byteArray.length);
    current = bytesRead;

    bufferOutput.write(byteArray, 0, current);
    bufferOutput.flush();
    System.out.println("Read " + current + " bytes");
    fileOutput.close();
  }

//  public static String getClientPath(String name) {
//    return "src/ClientFiles/" + name;
//  }
//
//  public static String getClientServerPath(String name) {
//    return "src/ServerFiles/" + name;
//  }

  /**
   * Adds current user's directory to the root, goes through all files in rootFile and inserts them into tree model
   * @param rootFile Given directory to be scanned
   * @param model Tree model
   * @param root Tree parent
   */

  protected static void addFiles(
      File rootFile, DefaultTreeModel model, DefaultMutableTreeNode root) {
    for (File file : Objects.requireNonNull(rootFile.listFiles())) {
      DefaultMutableTreeNode child = new DefaultMutableTreeNode(file.getName());
      model.insertNodeInto(child, root, root.getChildCount());
      if (file.isDirectory()) addFiles(file, model, child);
    }
  }

  /**
   * Updates GUI with the new content
   */
  public static void update() {
    DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
    root.removeAllChildren();
    model.reload();
    File rootFile = (File) root.getUserObject();
    addFiles(rootFile, model, root);
    tree.expandPath(new TreePath(root));
  }

  /**
   * Defined layout of the GUI
   */
  public class ClientFileBrowser extends JPanel {
    public ClientFileBrowser() {
      setLayout(new BorderLayout());
      tree = new JTree();
      File rootFile = new File(userPath);
      DefaultMutableTreeNode root = new DefaultMutableTreeNode(rootFile);
      model = new DefaultTreeModel(root);
      tree.setModel(model);
      tree.setRootVisible(true);
      tree.setShowsRootHandles(true);
      addFiles(rootFile, model, root);
      tree.addTreeSelectionListener(
          e -> {
            DefaultMutableTreeNode selectedNode =
                (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            lastSelectedNode = String.valueOf(selectedNode);
          });
      add(new JScrollPane(tree));
    }
  }

  /**
   * Gets list of all Clients that are actually connected to server
   * @param lista list of users
   */
  public void getRegisteredList(ArrayList<String> lista) {
    File rootFile = new File("src/ClientFiles");
    for (File file : Objects.requireNonNull(rootFile.listFiles())) {
      if (!username.equals(file.getName()) && file.isDirectory()) lista.add(file.getName());
    }
  }

  /**
   * Client's GUI with defined interactive functionality - buttons and combobox
   */
  public class ClientInteractiveGUI extends JPanel {
    public ClientInteractiveGUI() {
      listOfUsers = new ArrayList<>();

      JButton buttonReload = new JButton("Reload");
      JButton buttonSend = new JButton("Send");
      statusLabel = new JLabel("Checking...");
      getRegisteredList(listOfUsers);
      userList = new JComboBox(listOfUsers.toArray());

      if (userList.getItemCount() != 0) lastSelectedUser = userList.getSelectedItem().toString();
      userList.addActionListener(
          new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
              if (userList.getItemCount() != 0) {
                lastSelectedUser = userList.getSelectedItem().toString();
              }
            }
          });

      buttonReload.addActionListener(
          new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
              userList.removeAllItems();
              listOfUsers.clear();
              getRegisteredList(listOfUsers);
              remove(userList);
              userList = new JComboBox(listOfUsers.toArray());
              add(userList);
              if (userList.getItemCount() != 0)
                lastSelectedUser = userList.getSelectedItem().toString();
              userList.addActionListener(
                  new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                      if (userList.getItemCount() != 0) {
                        lastSelectedUser = userList.getSelectedItem().toString();
                      }
                    }
                  });
            }
          });

      buttonSend.addActionListener(
          new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent actionEvent) {
              String path = userPath;
              sendMessage("Shared file");
              sendMessage(lastSelectedUser.toString());
              sendMessage(lastSelectedNode.toString());
              try {
                sendFile(path, Paths.get(lastSelectedNode));
              } catch (IOException exc) {
                exc.printStackTrace();
              }

              JOptionPane.showMessageDialog(null, "Sent");
            }
          });
      add(statusLabel);
      add(buttonReload);
      add(buttonSend);
      add(userList);
    }
  }

  /**
   * Starts running window with GUI
   */
  public void startGUI() {
    frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle(username);
    frame.setPreferredSize(new Dimension(700, 500));
    frame.add(new Client.ClientFileBrowser(), BorderLayout.SOUTH);
    frame.add(new Client.ClientInteractiveGUI(), BorderLayout.NORTH);
    frame.addWindowListener(
        new WindowAdapter() {
          public void windowClosing(WindowEvent e) {
            stopConnection();
          }
        });
    frame.pack();
    frame.setVisible(true);
  }

  public static void main(String[] args) throws IOException {
    Client client = new Client();
    username = args[0];
    userPath = args[1];

    client.startConnection("127.0.0.1", 6666, username);

    Runnable dirClientListener =
        () -> {
          try {
            client.watchService(userPath);
          } catch (IOException e) {
            e.printStackTrace();
          }
        };

    Thread t = new Thread(dirClientListener, "dirClientListener");
    t.start();

    String MessageFromServer;
    String ShareFilename;
    String NewOnlineFile;
    while (true) {
      MessageFromServer = in.readLine();
      switch (MessageFromServer) {
        case ("Updating local directory"):
          client.statusLabel.setText("Downloading...");
          ShareFilename = in.readLine();
          saveFile(userPath + "/" + ShareFilename);
          update();
          break;

        case ("New online file"):
          client.statusLabel.setText("Sending...");
          NewOnlineFile = in.readLine();
          saveFile(userPath + "/" + NewOnlineFile);
          update();
          break;
      }
    }
  }
}
