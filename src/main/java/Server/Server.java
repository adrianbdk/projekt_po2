package Server;

import com.github.javafaker.Faker;

import javax.naming.directory.Attributes;
import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.net.*;
import java.io.*;
import java.nio.file.*;
import java.time.Duration;
import java.time.LocalTime;
import java.util.*;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ThreadLocalRandom;
import java.util.HashMap;
import java.util.Map;

import static java.nio.file.StandardWatchEventKinds.*;

public class Server {

  private ServerSocket serverSocket;

  private static String lastSelectedNode;

  private DefaultMutableTreeNode root;

  private static DefaultTreeModel model;

  private static JTree tree;

  static JFrame frame = new JFrame("Server Browser");

  public static ConcurrentHashMap<String, String> userList = new ConcurrentHashMap<>();
  private static final List<ClientHandler> HANDLER_LIST = new ArrayList<>();

  public Server() {
    startGUI();
  }

  public static void main(String[] args) throws IOException {
    Server server = new Server();
    server.start(6666);
  }

  /**
   * Starts server connection and establishes port that clients will connect to
   * @param port Number of port that clients will connect to
   * @throws IOException in case of problems with establishing connection/ serverSocket
   */
  public void start(int port) throws IOException {
    try {
      serverSocket = new ServerSocket(port);

      while (true) {
        new ClientHandler(serverSocket.accept()).start();
      }
    } catch (Exception exc) {
      System.out.println("ERROR - " + exc.getMessage());
    }
  }

  /**
   * Turns of the server
   * @throws IOException in case of problems with serverSocket
   */
  public void stop() throws IOException {
    try {
      serverSocket.close();
    } catch (Exception exc) {
      System.out.println("ERROR - " + exc.getMessage());
    }
  }

  /**
   * Updates GUI with new content
   */
  public void update() {
    DefaultMutableTreeNode root = (DefaultMutableTreeNode) model.getRoot();
    root.removeAllChildren();
    model.reload();
    File rootFile = (File) root.getUserObject();
    addFiles(rootFile, model, root);
    tree.expandPath(new TreePath(root));
  }

  /**
   * Adds directory to the tree including files
   * @param rootFile Given directory to be scanned
   * @param model Tree model
   * @param root Tree parent
   */
  protected static void addDirectory(
      File rootFile, DefaultTreeModel model, DefaultMutableTreeNode root) {
    for (File file : Objects.requireNonNull(rootFile.listFiles())) {
      DefaultMutableTreeNode child = new DefaultMutableTreeNode(file.getName());
      model.insertNodeInto(child, root, root.getChildCount());
      if (file.isDirectory()) addDirectory(file, model, child);
    }
  }

  /** Adds current user's directory to the root, goes through all files in rootFile and inserts them into tree model
   * @param rootFile Given directory to be scanned
   * @param model Tree model
   * @param root Tree parent
   */
  protected static void addFiles(
      File rootFile, DefaultTreeModel model, DefaultMutableTreeNode root) {
    for (File file : Objects.requireNonNull(rootFile.listFiles())) {
      if (file.isDirectory()) {
        for (Map.Entry<String, String> temp : userList.entrySet()) {
          String path = temp.getValue();
          if (path.equals(file.toString())) {
            DefaultMutableTreeNode child = new DefaultMutableTreeNode(file.getName());
            model.insertNodeInto(child, root, root.getChildCount());
            addDirectory(file, model, child);
            break;
          }
        }
      }
    }
  }

  /**
   * Defined server's GUI that ables to browse through current client's directories
   */
  public static class ServerFileBrowser extends JPanel {
    public ServerFileBrowser() {
      setLayout(new BorderLayout());
      tree = new JTree();
      File rootFile = new File("src/ServerFiles");
      DefaultMutableTreeNode root = new DefaultMutableTreeNode(rootFile);
      model = new DefaultTreeModel(root);
      tree.setModel(model);
      tree.setRootVisible(true);
      tree.setShowsRootHandles(true);
      addFiles(rootFile, model, root);
      tree.addTreeSelectionListener(
          e -> {
            DefaultMutableTreeNode selectedNode =
                (DefaultMutableTreeNode) tree.getLastSelectedPathComponent();
            lastSelectedNode = String.valueOf(selectedNode);
          });
      add(new JScrollPane(tree));
    }
  }

  /**
   * Starts running window with GUI
   */
  public static void startGUI() {
    frame = new JFrame();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setTitle("Server");
    frame.setPreferredSize(new Dimension(700, 500));
    frame.add(new ServerFileBrowser(), BorderLayout.SOUTH);
    frame.pack();
    frame.setVisible(true);
  }

  /**
   * Class defined for any actions related with Client
   */
  private class ClientHandler extends Thread {

    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;

    public ClientHandler(Socket socket) {
      this.clientSocket = socket;
    }

    /**
     * Saves given file using Input/Output Stream
     * @param filepath full path to the directory where file need to be saved
     * @throws IOException in case of problems with  Input/Output Stream
     */
    public void saveFile(String filepath) throws IOException {
      FileOutputStream fileOutput = new FileOutputStream(filepath);
      BufferedOutputStream bufferOutput = new BufferedOutputStream(fileOutput);
      int bytesRead;
      int current = 0;
      byte[] byteArray = new byte[8192];
      InputStream dataIn = clientSocket.getInputStream();
      bytesRead = dataIn.read(byteArray, 0, byteArray.length);
      System.out.println("Saving file");
      current = bytesRead;
      bufferOutput.write(byteArray, 0, current);
      bufferOutput.flush();
      System.out.println("Read " + current + " bytes");
      fileOutput.close();
      update();
    }

    /**
     * Sends given file to given directory using Input/Output stream
     * @param path path to the file that need to be sent
     * @param filename name of file that need to be send
     * @throws IOException in case of problems with  Input/Output Stream
     */
    public void sendFile(String path, Path filename) throws IOException {
      FileInputStream fileInput = null;
      BufferedInputStream bufferInput = null;
      OutputStream dataOut = null;
      File myFile = new File(path + File.separator + filename);
      byte[] byteArray = new byte[(int) myFile.length()];

      fileInput = new FileInputStream(myFile);
      bufferInput = new BufferedInputStream(fileInput);
      bufferInput.read(byteArray, 0, byteArray.length);
      dataOut = clientSocket.getOutputStream();
      System.out.println("Sending " + byteArray.length + " bytes");

      dataOut.write(byteArray, 0, byteArray.length);
      System.out.println("Done");
      bufferInput.close();
    }

    /**
     * Sends message to Client using PrintWriter
     * @param msg message to be send to Server
     */

    public void sendMessage(String msg) {
      try {
        out.println(msg);
      } catch (Exception exc) {
        System.out.println("ERROR - " + exc);
      }
    }

    /**
     * Compares contents of Client's server and local directory, and sends back missing files to the Client's local folder
     *
     * @param ClientName name of the client that directories need to be compared
     * @throws InterruptedException in case of null values
     * @throws IOException in case of problems with sending files/messages
     */
    public void missingFilesHandler(String ClientName) throws InterruptedException, IOException {
      File ServerDir = new File(getServerPath(ClientName));
      File ClientDir = new File(getLocalPath(ClientName));
      Thread.sleep(1000);

      for (File ServerFile : Objects.requireNonNull(ServerDir.listFiles())) {
        boolean missingFile = true;
        for (File ClientFile : Objects.requireNonNull(ClientDir.listFiles())) {
          if (ServerFile.getName().equals(ClientFile.getName())) {
            missingFile = false;
          }
        }
        if (missingFile) {
          Thread.sleep(100);
          sendMessage("Updating local directory");
          sendMessage(ServerFile.getName());
          sendFile(getServerPath(ClientName), Paths.get(ServerFile.getName()));
        }
      }
    }

    /**
     * Checks if given folder exists
     * @param folderName folder to be checked
     * @return false if folder doesn't exist, true if exists
     */
    public boolean checkIfFolderExists(String folderName) {

      boolean found = false;

      try {
        File file = new File(folderName);
        if (file.exists() && file.isDirectory()) {
          found = true;
        }
      } catch (Exception e) {
        e.printStackTrace();
      }

      return found;
    }

    /**
     * Checks if Client's directory extists, if not creates it and updates GUI with new content
     * @param ClientUsername
     * @throws IOException
     */
    public void clientDirHandler(String ClientUsername) throws IOException {
      Path rootDirectory = FileSystems.getDefault().getPath("src/ServerFiles");
      String expectedDir = rootDirectory + "/" + ClientUsername;

      if (checkIfFolderExists(expectedDir)) {
        System.out.println("Folder present: " + checkIfFolderExists(expectedDir));
        System.out.println(expectedDir);
      } else {
        System.out.println("Folder present: " + checkIfFolderExists(expectedDir));
        Path userDirectory = Files.createDirectory(Paths.get(rootDirectory + "/" + ClientUsername));
        System.out.println(userDirectory);
        update();
      }
    }

    /**
     * Deletes file from given path
     * @param filepath full path to the file that needs to be deleted
     */
    public void deleteFile(String filepath) {
      File file = new File(filepath);

      if (file.delete()) {
        System.out.println("File deleted successfully");
      } else {
        System.out.println("Failed to delete the file");
      }
    }

    /**
     * Ends Server's connection
     */
    public void KillConnection() {
      try {
        in.close();
        out.close();
        clientSocket.close();
      } catch (IOException exc) {
        System.out.println("ERROR - " + exc);
      }
    }

    /**
     * Function monitors for any changes in given directory
     * @param path path to directory that needs to be monitored
     * @throws IOException in case of problems with sending message or running watcher
     */

    public void watchService(String path, String ClientName) throws IOException {
      WatchService watcher = FileSystems.getDefault().newWatchService();
      Path dir = Paths.get(path);
      dir.register(watcher, ENTRY_CREATE, ENTRY_DELETE, ENTRY_MODIFY);

      while (true) {
        WatchKey key;
        try {
          // wait for a key to be available
          key = watcher.take();
        } catch (InterruptedException ex) {
          return;
        }

        for (WatchEvent<?> event : key.pollEvents()) {
          // get event type
          WatchEvent.Kind<?> kind = event.kind();

          // get file name
          @SuppressWarnings("unchecked")
          WatchEvent<Path> ev = (WatchEvent<Path>) event;
          Path fileName = ev.context();

          if (kind == OVERFLOW) {
            continue;
          } else if (kind == ENTRY_CREATE) {
            update();
            sendMessage("New online file");
            sendMessage(fileName.toString());
            sendFile(getServerPath(ClientName), fileName);

          } else if (kind == ENTRY_DELETE) {
            System.out.println("Deleting " + path + File.pathSeparator + fileName);
            update();

          } else if (kind == ENTRY_MODIFY) {

          }
        }
        // IMPORTANT: The key must be reset after processed
        boolean valid = key.reset();
        if (!valid) {
          break;
        }
      }
    }

    /**
     * Returns path to the Client's Server path
     * @param name name of the Client
     * @return Server directory
     */
    public String getServerPath(String name) {
      return "src/ServerFiles/" + name;
    }

    /**
     * Return path to the Client's local directory
     * @param name name of the Client
     * @return Local directory
     */
    public String getLocalPath(String name) {
      return "src/ClientFiles/" + name;
    }

    /**
     * Runs all the processes and listens for any incomming messages from Client
     */
    public void run() {
      try {
        out = new PrintWriter(clientSocket.getOutputStream(), true);

        in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));

        String username = in.readLine();
        userList.put(username, getServerPath(username));
        update();
        clientDirHandler(username);
        missingFilesHandler(username);

        Runnable dirClientListener =
            () -> {
              try {
                watchService(getServerPath(username), username);
              } catch (IOException e) {
                e.printStackTrace();
              }
            };

        Thread t = new Thread(dirClientListener, "dirServerListener");
        t.start();

        while (true) {
          if (clientSocket.isClosed()) break;
          String ClientNotification = in.readLine();
          String FileName;
          String ShareUsername;
          String ShareFilename;
          String SharePath;

          switch (ClientNotification) {
            case ("New File"):
              FileName = in.readLine();
              saveFile(getServerPath(username) + "/" + FileName);
              break;
            case ("Deleted File"):
              FileName = in.readLine();
              deleteFile(getServerPath(username) + "/" + FileName);
              update();
              break;
            case ("Disconnected"):
              System.out.println("Closing connection with " + username);
              userList.remove(username);
              KillConnection();
              update();
              break;
            case ("Shared file"):
              ShareUsername = in.readLine();
              ShareFilename = in.readLine();
              System.out.println("Sharing file with: " + ShareUsername);
              SharePath = getServerPath(ShareUsername) + "/" + ShareFilename;
              saveFile(SharePath);
              update();
              break;
          }
        }

      } catch (IOException | InterruptedException exc) {
        System.out.println("ERROR - " + exc.getMessage());
      }
    }
  }
}
